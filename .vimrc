"installed plugins:
"   * pathogen
"   * SnipMate: create snippets in .vim/snippets
"       -> http://www.bestofvim.com/plugin/snipmate/
"   * NerdTree: :NERDTree

"Set the vim folder correct
let $VIM='~/.vim/'

"pathogen plugin insaller
"save pathogen.vim to ~/.vim/autoload
"
"call pathogen#infect()

" Now just download the plugin you want to install to ~/.vim/bundle/<new plugin>

filetype plugin indent on

syntax on

"Colorscheme
colorscheme solarized
set background=dark

"Set mapleader from \ to ,
let mapleader=","

"---------------------
" => Basic settings <=
"---------------------
set mouse=a		" Enable use of the mouse for all modes
"set mousemodel=popup
set pastetoggle=<F2> 	"toogles to past text with right click
"set autoread            " Set to auto read when a file is changed from the outside
" ^ works bad while saving files

set nocompatible  	"Uses Vim Settings
set hidden 		"enables different useful features
set showcmd		"display incomplete commands
set wildmenu
set wildmode=list:longest
set wildignore =*.swp,*.o,*~,*.pyc "ignore some extensions in completing names

set novisualbell          "no beeps!!!

set number		"row numbers
set numberwidth=4
set scrolloff=10        "keep 10 lines for scope 
set ruler		"show the cursor position all the time
set guioptions-=T	"Hide Toolbar
set textwidth=100

set autochdir 		"always switch to the current file directory 
set nobackup     	"dont make backup files
set noswapfile 		"no swapfile... 
set backupdir =~/.vim/backup  "where to put backupfiles
set directory =~/.vim/tmp

set history=1000 	"remember more history
set undolevels=1000 	"more undo

set ignorecase		"case insensitive search...
set smartcase		" ...except when using capital letters

set iskeyword +=_,$,%,#,. "none of these are word dividers
set backspace=indent,eol,start 	" allow backspacing over everything in insert mode
set expandtab      	"Will insert softtabstop amount of spaces
set shiftwidth=4   	"effects how <<, >> and == work
set softtabstop=4

set foldmethod=marker   " folding with {{{ }}}

" Tell vim to remember certain things when we exit
"  '10  :  marks will be remembered for up to 10 previously edited files
"  "100 :  will save up to 100 lines for each register
"  :20  :  up to 20 lines of command-line history will be remembered
"  %    :  saves and restores the buffer list
"  n... :  where to save the viminfo files
"set viminfo='10,\"100,:20,%,n~/.viminfo 

"----------------------
" => Simple Mappings <=
"----------------------

"prev tab
nnoremap gr gT

nnoremap t :tabnew 

"let j and k go line by line
nnoremap j gj
nnoremap k gk

" Easy window navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

"Create blank new lines and stay in normal mode
nnoremap <silent> zj o<ESC>
nnoremap <silent> zk O<ESC>

"Move lines
nnoremap - ddkP
nnoremap _ ddp

"clears line
nnoremap dc ddO

"shifting
nnoremap <leader>< <<
nnoremap <leader>y >>

" copy rest of the line to upper line
nnoremap <leader>pk <ESC>v$hdO<ESC>pI

"select last insertion
nnoremap <leader>v V'[


"-----------------------
" => Bracket Mappings <=
"-----------------------

"insert [{brackets}]  
nnoremap <C-e> a[]<ESC>i
nnoremap <C-s> a{}<ESC>i
inoremap <C-e> <ESC>a[]<ESC>i
inoremap <C-s> <ESC>a{}<ESC>i

"insert around word
nnoremap <leader>" ea"<ESC>hbi"<ESC>lwl
nnoremap <leader>' ea'<ESC>hbi'<ESC>lwl
nnoremap <leader>( ea)<ESC>hbi(<ESC>lwl
nnoremap <leader>{ ea}<ESC>hbi{<ESC>lwl
nnoremap <leader>[ ea]<ESC>hbi[<ESC>lwl

"in NEXT bracket / in LAST bracket
onoremap in( :<c-u>normal! f(lvi(<cr>
onoremap il( :<c-u>normal! F)hvi(<cr>

onoremap in" :<c-u>normal! f"lvi"<cr>
onoremap il" :<c-u>normal! F"hvi"<cr>

onoremap in{ :<c-u>normal! f{lvi}<cr>
onoremap il{ :<c-u>normal! F}hvi{<cr>

onoremap in[ :<c-u>normal! f[lvi]<cr>
onoremap il[ :<c-u>normal! F]hvi[<cr>
"
"-----------------------
" => Specific Mappings <=
"-----------------------

"Geant4 separation
nnoremap <leader>gk o//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......<ESC>j


"------------------------
" => Advanced Mappings <=
"------------------------

" Quickly edit/reload the vimrc file
nnoremap <leader>ev :tabnew $MYVIMRC<CR>
nnoremap <leader>sv :so $MYVIMRC<CR>

" <leader>lt -> go to last active tab
let g:lasttab = 1
nmap <Leader>lt :exe "tabn ".g:lasttab<CR>
if has("autocmd")
    au TabLeave * let g:lasttab = tabpagenr()
endif


"------------------------
" => Advanced Commands <=
"------------------------

"Let vim automatically restore foldings
" -> Hat schonmal Probleme gemacht... 
if has("autocmd")
"    au BufWinLeave,BufWritePost * mkview
    au BufWritePost * mkview
    au BufWinEnter *.* silent loadview
endif

"Not needed
"if has("autocmd")
"  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
"endif


"------------
" => LaTeX <=
"------------

let g:Tex_ViewRule_pdf = "Okular"

" IMPORTANT: grep will sometimes skip displaying the file name if you
" search in a singe file. This will confuse Latex-Suite. Set your grep
" program to always generate a file-name.
set grepprg=grep\ -nH\ $*

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
let g:tex_flavor='pdflatex'

"nnoremap <leader>lb ,ll:!bibtex  

"--- Plugins ---
nnoremap <silent> <F3> :YRShow<cr>
inoremap <silent> <F3> <ESC>:YRShow<cr>

nnoremap <leader>n :NERDTree .<CR>
nnoremap <leader>N :NERDTreeClose<CR>
"ignore these files on shortcut f
let NERDTreeIgnore=['\.root$', '\.h$', '\.o$', '\.gmk$', '\.mac$', '\.conf', '\.bin', 'README$', '\~$', 'Makefile$', '\.inc$']

set rtp+=~/.vam/vim-addon-manager
call vam#ActivateAddons(["FuzzyFinder"])
"Great
call vam#ActivateAddons(["AutoComplPop"])
call vam#ActivateAddons(["snipmate"])
call vam#ActivateAddons(["Command-T"])
"NERDTree
call vam#ActivateAddons(["nerdtree-execute"])
call vam#ActivateAddons(["ack"])
"CTRL-P for Yankring
call vam#ActivateAddons(["YankRing"])
call vam#ActivateAddons(["rainbow_parentheses"])
"<leader><leader>w or <leader><leader>f
call vam#ActivateAddons(["EasyMotion"])
call vam#ActivateAddons(["project.tar.gz"])
":A switches from .h to .cpp and vice versa
call vam#ActivateAddons(["a"])
"TlistToggle for list in c files
call vam#ActivateAddons(["taglist"])
